package com.mobanode.social.entity;


import com.mobanode.social.SocialConfig;
import com.mobanode.social.SocialUtils;
import com.mobanode.social.SocialCache;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.json.DataObjectFactory;

public class Tweet {

    private String tweetText;
    private String tweetCreated;
    private String tweetId;
    private boolean isRetweet;

    public static final String TWITTER_NAME = "scottmahermusic";
    public static final String TWEET_URL = "https://mobile.twitter.com/" + TWITTER_NAME + "/status/";

    public Tweet(String tweetText, String tweetCreated, String tweetId, boolean isRetweet) {
        super();
        this.tweetText = tweetText;
        this.tweetCreated = tweetCreated;
        this.tweetId = tweetId;
        this.isRetweet = isRetweet;
    }

    public String getTweetId() {
        return tweetId;
    }

    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    public String getTweetText() {
        return tweetText;
    }

    public void setTweetText(String tweetText) {
        this.tweetText = tweetText;
    }

    public String getTweetCreated() {
        return tweetCreated;
    }

    public void setTweetCreated(String tweetCreated) {
        this.tweetCreated = tweetCreated;
    }

    public boolean isRetweet() {
        return isRetweet;
    }

    public void setRetweet(boolean isRetweet) {
        this.isRetweet = isRetweet;
    }

    public static String getNewTwitterFeed() {
        ConfigurationBuilder builder = new ConfigurationBuilder();

        builder.setOAuthConsumerKey(SocialConfig.FEED_CONSUMER_KEY);
        builder.setOAuthConsumerSecret(SocialConfig.FEED_CONSUMER_SECRET);
        builder.setOAuthAccessToken(SocialConfig.FEED_ACCESS_TOKEN);
        builder.setOAuthAccessTokenSecret(SocialConfig.FEED_ACCESS_TOKEN_SERCRET);
        builder.setJSONStoreEnabled(true);
        builder.setIncludeEntitiesEnabled(true);
        builder.setIncludeMyRetweetEnabled(true);
        builder.setIncludeRTsEnabled(true);

        AccessToken accessToken = new AccessToken(SocialConfig.FEED_ACCESS_TOKEN, SocialConfig.FEED_ACCESS_TOKEN_SERCRET);
        Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

        try {
            @SuppressWarnings("unchecked")
            ArrayList<Status> statuses = (ArrayList<Status>) twitter.getUserTimeline(TWITTER_NAME);
            return DataObjectFactory.getRawJSON(statuses);
        } catch (TwitterException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* Parse Twitter JSON */
    public static void parseTwitterJson(String jsonStr) {
        ArrayList<Tweet> tweetsList = new ArrayList<Tweet>();
        try {
            JSONArray array = new JSONArray(jsonStr);

            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObj = array.getJSONObject(i);

                String tweetText = jsonObj.getString("text");
                String tweetDate = SocialUtils.processSocialPostDate(jsonObj.getString("created_at"), "twitter");
                String tweetId = jsonObj.getString("id_str");
                boolean isRetweet = jsonObj.has("retweeted_status");
                tweetsList.add(new Tweet(tweetText, tweetDate, tweetId, isRetweet));
            }

            SocialCache.twitterPosts = tweetsList;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
