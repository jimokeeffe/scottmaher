package com.mobanode.social.entity;

import android.util.Log;

import com.mobanode.social.SocialCache;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class YoutubePost {

    public static final String YT_NAME = "Scott7Maher";
    private String title;
    private String vidUrl;
    private String thumbUrl;
    private int numViews;

    public YoutubePost() {
        super();
    }

    public YoutubePost(String title, String vidUrl, String thumbUrl,
                       int numViews) {
        super();
        this.title = title;
        this.vidUrl = vidUrl;
        this.thumbUrl = thumbUrl;
        this.numViews = numViews;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVidUrl() {
        return vidUrl;
    }

    public void setVidUrl(String vidUrl) {
        this.vidUrl = vidUrl;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public int getNumViews() {
        return numViews;
    }

    public void setNumViews(int numViews) {
        this.numViews = numViews;
    }

    public static void parseYoutubeJSON(String json) {
        SocialCache.youtubePosts = new ArrayList<YoutubePost>();
        SocialCache.ytImgUrls = new ArrayList<String>();

        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONObject dataObj = jsonObj.getJSONObject("data");
            JSONArray itemsArray = dataObj.getJSONArray("items");
            YoutubePost ytPost;

            for (int i = 0; i < itemsArray.length(); i++) {
                JSONObject item = itemsArray.getJSONObject(i);
                JSONObject video = item.getJSONObject("video");

                if (!video.has("status")) {
                    ytPost = new YoutubePost();

                    ytPost.setTitle(video.getString("title"));

                    if (video.has("viewCount"))
                        ytPost.setNumViews(video.getInt("viewCount"));

                    if (video.has("thumbnail")) {
                        JSONObject thumbObj = video.getJSONObject("thumbnail");
                        if (thumbObj.has("sqDefault")) {
                            ytPost.setThumbUrl(thumbObj.getString("sqDefault"));
                            SocialCache.ytImgUrls.add(ytPost.getThumbUrl());
                        }
                    }

                    if (video.has("player")) {
                        JSONObject playerObj = video.getJSONObject("player");
                        if (playerObj.has("default")) {
                            ytPost.setVidUrl(playerObj.getString("default"));
                        }
                    }

                    SocialCache.youtubePosts.add(ytPost);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void parseYouTubeV3Json(String json) {
        final String YT_VIDEO_BASE_URL = "https://www.youtube.com/watch?v=";
        SocialCache.youtubePosts = new ArrayList<YoutubePost>();
        SocialCache.ytImgUrls = new ArrayList<String>();

        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONArray itemsArray = jsonObj.getJSONArray("items");

            for (int i = 0; i < itemsArray.length(); i++) {
                JSONObject item = itemsArray.getJSONObject(i);
                YoutubePost ytPost = new YoutubePost();

                if (item.has("id")) {
                    JSONObject resourceIdObj = item.getJSONObject("id");

                    if (resourceIdObj.has("kind")) {
                        String kind = resourceIdObj.getString("kind");
                        if (kind.contains("video")) {
                            if (resourceIdObj.has("videoId")) {
                                ytPost.setVidUrl(YT_VIDEO_BASE_URL + resourceIdObj.getString("videoId"));
                            }

                            if (item.has("snippet")) {
                                JSONObject snippetObj = item.getJSONObject("snippet");
                                ytPost.setTitle(snippetObj.getString("title"));
                                Log.v("title", snippetObj.getString("title"));
                                Log.v("title", ytPost.getTitle());

                                if (snippetObj.has("thumbnails")) {
                                    JSONObject thumbObj = snippetObj.getJSONObject("thumbnails");
                                    if (thumbObj.has("medium")) {
                                        JSONObject urlObj = thumbObj.getJSONObject("medium");
                                        ytPost.setThumbUrl(urlObj.getString("url"));

                                        SocialCache.ytImgUrls.add(ytPost.getThumbUrl());
                                        SocialCache.youtubePosts.add(ytPost);
                                    }
                                }}
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
