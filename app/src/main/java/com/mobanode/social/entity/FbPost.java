package com.mobanode.social.entity;

import android.util.Patterns;


import com.mobanode.social.SocialUtils;
import com.mobanode.social.SocialCache;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FbPost {

    private String postType;
    private String postDate;
    private String message;
    private String thumb;
    private String link;
    private String linkName;
    private String postID;

    public static final String APP_ID = "1513086745575295";
    public static final String APP_SECRET = "941236164e9f6e4a180c33d83a0120ef";
    public static final String PAGE_ID = "522410874485215"; // Scott maher

    // Constructor
    public FbPost(String postType, String postDate, String message,
                  String thumb, String link, String linkName, String postID) {
        super();
        this.postType = postType;
        this.postDate = postDate;
        this.message = message;
        this.thumb = thumb;
        this.link = link;
        this.linkName = linkName;
        this.postID = postID;
    }

    // Getters and setters
    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.link = linkName;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public static void parseFBJson(String json) {
        ArrayList<FbPost> fbPosts = new ArrayList<FbPost>();
        ArrayList<String> imgUrls = new ArrayList<String>();

        try {
            JSONObject jsonObj = new JSONObject(json);
            JSONArray jArray = jsonObj.getJSONArray("data");

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject obj = jArray.getJSONObject(i);

                if (!obj.has("story") && obj.has("message")) {
                    String link, thumb, message, linkName;
                    link = thumb = message = linkName = "";

                    String type = obj.getString("type");
                    String postID = obj.getString("id").split("_")[1];
//					Log.v("FB", "id: " + postID);
                    String postDate = SocialUtils.processSocialPostDate(obj.getString("created_time"), "facebook");

                    if (obj.has("message")) {
                        message = obj.getString("message");
//						Log.v("FB", "msg: " + message);
                    }

                    if (type.equals("link")) {
                        if (obj.has("link"))
                            link = obj.getString("link");
                        else {
                            Pattern p = Pattern.compile(Patterns.WEB_URL.toString());
                            Matcher m = p.matcher(obj.getString("message"));

                            while (m.find())
                                link = m.group();
                        }

                        if (obj.has("name"))
                            linkName = obj.getString("name");

                        if (obj.has("picture")) {
                            thumb = obj.getString("picture");
                            imgUrls.add(thumb);
                        }
                    }
                    if (type.equals("photo")) {
                        if (obj.has("picture")) {
                            thumb = obj.getString("picture");
                            imgUrls.add(thumb);
                        }

                        if (obj.has("link"))
                            link = obj.getString("link");
                    } else if (type.equals("video")) {
                        if (obj.has("picture")) {
                            thumb = obj.getString("picture");
                            imgUrls.add(thumb);
                        }

                        if (obj.has("link"))
                            link = obj.getString("link");

                        if (obj.has("name"))
                            linkName = obj.getString("name");
                    }

                    FbPost post = new FbPost(type, postDate, message, thumb, link, linkName, postID);
                    fbPosts.add(post);
                }
            }

            SocialCache.facebookPosts = fbPosts;
            SocialCache.fbImgUrls = imgUrls;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
