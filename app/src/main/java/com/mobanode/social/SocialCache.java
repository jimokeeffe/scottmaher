package com.mobanode.social;

import android.graphics.Bitmap;

import com.mobanode.social.entity.FbPost;
import com.mobanode.social.entity.Tweet;
import com.mobanode.social.entity.YoutubePost;

import java.util.ArrayList;
import java.util.HashMap;


public class SocialCache {

    public static ArrayList<FbPost> facebookPosts;
    public static ArrayList<Tweet> twitterPosts;
    public static ArrayList<String> fbImgUrls;
    public static ArrayList<YoutubePost> youtubePosts;
    public static ArrayList<String> ytImgUrls;
    public static HashMap<String, Bitmap> ytImageCache = new HashMap<String, Bitmap>();
    public static HashMap<String, Bitmap> fbImageCache;
}
