package com.mobanode.social.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobanode.scottmaher.R;
import com.mobanode.social.SocialCache;
import com.mobanode.social.entity.FbPost;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class FBListAdapter extends ArrayAdapter<FbPost> {

    private ArrayList<FbPost> posts = new ArrayList<FbPost>();
    private ArrayList<String> imgUrls = new ArrayList<String>();
    private LayoutInflater vi;
    private int layoutId;

    public FBListAdapter(Context context, int textViewResourceId, ArrayList<FbPost> posts, ArrayList<String> imgUrls) {
        super(context, textViewResourceId, posts);
        this.layoutId = textViewResourceId;
        this.posts = posts;
        this.vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imgUrls = imgUrls;

        if (SocialCache.fbImageCache == null)
            SocialCache.fbImageCache = new HashMap<String, Bitmap>();

        new DownloadThumbs().execute();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Reset view and inflate new view
//        convertView = null;
        convertView = vi.inflate(layoutId, null);

        // Find Facebook post for current position and set layout based on type of post
        FbPost post = posts.get(position);
        if (post != null) {
            final TextView msgText = (TextView) convertView.findViewById(R.id.fbMessageTextView);
            final TextView linkName = (TextView) convertView.findViewById(R.id.fbLinkNameTextView);
//            final TextView linkText = (TextView) convertView.findViewById(R.id.fbLinkTextView);
            final TextView postDate = (TextView) convertView.findViewById(R.id.fbDateTextView);
            final ImageView thumb = (ImageView) convertView.findViewById(R.id.fbThumbImgView);

            if (post.getPostType().equals("link")) {
                msgText.setText(post.getMessage());
                postDate.setText(post.getPostDate());
                linkName.setText(post.getLinkName());
//                linkText.setText(post.getLink());
            } else if (post.getPostType().equals("photo")) {
                msgText.setText(post.getMessage());
                postDate.setText(post.getPostDate());

                if (SocialCache.fbImageCache.containsKey(post.getThumb())) {
                    thumb.setVisibility(View.VISIBLE);
                    thumb.setImageBitmap(SocialCache.fbImageCache.get(post.getThumb()));
                }
            } else if (post.getPostType().equals("status")) {
                msgText.setText(post.getMessage());
                postDate.setText(post.getPostDate());
            } else if (post.getPostType().equals("video")) {
                msgText.setText(post.getMessage());
                postDate.setText(post.getPostDate());
                linkName.setText(post.getLinkName());

                if (SocialCache.fbImageCache.containsKey(post.getThumb())) {
                    thumb.setVisibility(View.VISIBLE);
                    thumb.setImageBitmap(SocialCache.fbImageCache.get(post.getThumb()));
                }
            }

            if (linkName.getText().toString().equals(""))
                linkName.setVisibility(View.GONE);
//            if (linkText.getText().toString().equals(""))
//                linkText.setVisibility(View.GONE);
            if (post.getMessage().equals(""))
                msgText.setVisibility(View.GONE);
        }
        return convertView;
    }

    private void notifyListOfChange() {
        this.notifyDataSetChanged();
    }

    /* Asynchronous task to download and cache all thumbnails */
    public class DownloadThumbs extends AsyncTask<Void, Void, Void> {
        private Bitmap thumbImg = null;

        @Override
        protected Void doInBackground(Void... params) {
            for (String url : imgUrls) {
                if (!SocialCache.fbImageCache.containsKey(url)) {
                    try {
                        URL thumbUrl = new URL(url);
                        HttpURLConnection conn = (HttpURLConnection) thumbUrl.openConnection();
                        InputStream is = conn.getInputStream();
                        thumbImg = BitmapFactory.decodeStream(is);
                        SocialCache.fbImageCache.put(url, thumbImg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    publishProgress();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            notifyListOfChange();
            super.onProgressUpdate(values);
        }
    }

}
