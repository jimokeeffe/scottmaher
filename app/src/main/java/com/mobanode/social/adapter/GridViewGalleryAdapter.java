package com.mobanode.social.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.mobanode.ScottMaherApplication;
import com.mobanode.scottmaher.R;
import com.mobanode.scottmaher.util.CommonUtils;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import java.util.ArrayList;

/**
 * Created by Jims on 15/09/2014.
 */
public class GridViewGalleryAdapter extends BaseAdapter {

    private final int widthInDP = 150;
    private final int paddingInPx = 1;
    private final int heightInDP = 90;

    private int thumbWidth;
    private int thumbHeight;
    private int thumbPadding;

    private Context ctx;
    private ArrayList<String> thumbUrls;

    private ImageTagFactory imgTagFactory;
    private ImageManager imageLoader = ScottMaherApplication.getImageManager();

    public GridViewGalleryAdapter(Context ctx, ArrayList<String> thumbUrls) {
        this.ctx = ctx;
        this.thumbUrls = thumbUrls;

        thumbWidth = CommonUtils.getPixelsFromPadding(ctx, widthInDP);
        thumbPadding = CommonUtils.getPixelsFromPadding(ctx, paddingInPx);
        thumbHeight = CommonUtils.getPixelsFromPadding(ctx, heightInDP);

        this.imgTagFactory = ImageTagFactory.newInstance(ctx, R.drawable.grid_holder);
        this.imgTagFactory.setErrorImageId(R.drawable.grid_holder);
    }

    @Override
    public int getCount() {
        return thumbUrls.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;

        if(convertView == null) {
            imageView = new ImageView(ctx);
            imageView.setBackgroundColor(ctx.getResources().getColor(R.color.dark_grey_color));
            imageView.setLayoutParams(new GridView.LayoutParams(thumbWidth, thumbWidth));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(thumbPadding, thumbPadding, thumbPadding, thumbPadding);
        }
        else {
            imageView = (ImageView) convertView;
        }
    Log.v("urls", thumbUrls.get(position));

        ImageTag imgTag = imgTagFactory.build(thumbUrls.get(position), ctx);
        imageView.setTag(imgTag);
        imageLoader.getLoader().load(imageView);

        return imageView;
    }

}

