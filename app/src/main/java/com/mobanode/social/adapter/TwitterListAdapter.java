package com.mobanode.social.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobanode.scottmaher.R;
import com.mobanode.social.entity.Tweet;

import java.util.ArrayList;

public class TwitterListAdapter extends ArrayAdapter<Tweet> {

    private Context mContext;
    private ArrayList<Tweet> tweets;
    private int layoutId;

    public TwitterListAdapter(Context context, int textViewResourceId, ArrayList<Tweet> tweets) {
        super(context, textViewResourceId, tweets);
        this.layoutId = textViewResourceId;
        mContext = context;
        this.tweets = tweets;
    }

    @Override
    public int getCount() {
        try {
            return tweets.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(layoutId, null);

            ViewHolder holder = new ViewHolder();
            holder.tweetLayout = (LinearLayout) convertView.findViewById(R.id.tweetLayout);
            holder.tweetText = (TextView) convertView.findViewById(R.id.tweetTextView);
            holder.tweetDate = (TextView) convertView.findViewById(R.id.tweetCreatedTextView);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tweetText.setText(Html.fromHtml(tweets.get(position).getTweetText()));
        holder.tweetDate.setText(tweets.get(position).getTweetCreated());

        return convertView;
    }

    private class ViewHolder {
        public LinearLayout tweetLayout;
        public TextView tweetText;
        public TextView tweetDate;
    }

}
