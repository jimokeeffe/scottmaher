package com.mobanode.social.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobanode.scottmaher.R;
import com.mobanode.social.SocialCache;
import com.mobanode.social.entity.YoutubePost;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class YouTubeListAdapter extends ArrayAdapter<YoutubePost> {

    private ArrayList<YoutubePost> posts = new ArrayList<YoutubePost>();
    private Context mCtx;
    private Drawable imagePlaceholder;
    private int layoutId;

    private LayoutInflater inflater;

    public YouTubeListAdapter(Context context, int textViewResourceId, ArrayList<YoutubePost> ytPosts) {
        super(context, textViewResourceId, ytPosts);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.layoutId = textViewResourceId;
        this.posts = ytPosts;
        this.mCtx = context;
        this.imagePlaceholder = context.getResources().getDrawable(R.drawable.yt_placeholder);

        if (SocialCache.ytImageCache == null)
            SocialCache.ytImageCache = new HashMap<String, Bitmap>();

        new DownloadThumbs().execute();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(layoutId, null);

            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.ytVideoTitleTextView);
            holder.thumb = (ImageView) convertView.findViewById(R.id.ytThumbImageView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        YoutubePost post = posts.get(position);
        if (post != null) {
            holder.title.setText(post.getTitle());
//            views.setText(post.getNumViews() + " views");

            if (SocialCache.ytImageCache.containsKey(post.getThumbUrl()))
                holder.thumb.setImageBitmap(SocialCache.ytImageCache.get(post.getThumbUrl()));
            else
                holder.thumb.setImageDrawable(imagePlaceholder);
        }

        return convertView;
    }

    private class ViewHolder {
        TextView title;
        //        TextView views;
        ImageView thumb;
    }

    private void notifyListOfChange() {
        this.notifyDataSetChanged();
    }


    /* Asynchronous task to download and cache all thumbnails */
    public class DownloadThumbs extends AsyncTask<Void, Void, Void> {
        private Bitmap thumbImg = null;

        @Override
        protected Void doInBackground(Void... params) {
            for (String url : SocialCache.ytImgUrls) {
                if (!SocialCache.ytImageCache.containsKey(url)) {
                    try {
                        URL thumbUrl = new URL(url);
                        HttpURLConnection conn = (HttpURLConnection) thumbUrl.openConnection();
                        InputStream is = conn.getInputStream();
                        thumbImg = BitmapFactory.decodeStream(is);
                        SocialCache.ytImageCache.put(url, thumbImg);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    publishProgress();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            notifyListOfChange();
            super.onProgressUpdate(values);
        }
    }

}
