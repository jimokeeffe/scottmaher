package com.mobanode.social.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.mobanode.scottmaher.R;
import com.mobanode.social.entity.Tweet;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class SocialDialogFragment extends DialogFragment {


    public SocialDialogFragment(){

    }

    public static SocialDialogFragment newInstance(String tweetID){
        Bundle bundle = new Bundle();
        bundle.putString("tweetId", tweetID);
        SocialDialogFragment fragment = new SocialDialogFragment();
        fragment.setArguments(bundle);

        return fragment;
    }





    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_layout_social, null);
        final String URL = Tweet.TWEET_URL + getArguments().getString("tweetId");
        final ProgressBar progBar = (ProgressBar) view.findViewById(R.id.loadPostProgressBar);
        WebView socWebView = (WebView) view.findViewById(R.id.socialWebView);
        socWebView.getSettings().setJavaScriptEnabled(true);
        socWebView.getSettings().setDomStorageEnabled(true);
        socWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        socWebView.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);
        socWebView.clearCache(true);

        socWebView.setWebViewClient(new WebViewClient() {

            private boolean isLoaded = false;

            @Override
            public void onPageFinished(WebView view, String url) {
                progBar.setVisibility(ProgressBar.GONE);
                isLoaded = true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (isLoaded) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                }
                return false;
            }
        });

        socWebView.loadUrl(URL);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }


}
