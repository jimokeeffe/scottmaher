package com.mobanode.social;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by mikenolan on 10/04/2014.
 */
public class SocialUtils {


    /**
     * Parse Application ID
     */
    public static final String PARSE_APPLICATION_ID = "6FCXpRXzxXb5f7ceZ3694tBgPojJSK0x0w7WNMU9";

    /**
     * Parse Client Key
     */
    public static final String PARSE_CLIENT_KEY = "NuxTHKuqdEIAvIEDmLFRrRIpUzgZm7zx9WXLyVWR";


    /**
     * Process Titter / Facebook post dates for readability in list
     *
     * @param dateStr  - Date String from feed
     * @param postType - Post type "twitter" or "facebook"
     * @return Returns formatted date as String
     */
    public static String processSocialPostDate(String dateStr, String postType) {
        SimpleDateFormat dateFormat;
        if (postType.equals("twitter"))
            dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.getDefault());//SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        else // Facebook
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.getDefault());

        long date = 0;

        try {
            date = dateFormat.parse(dateStr).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Date dateCreated = new Date(date + TimeZone.getDefault().getRawOffset() + (TimeZone.getDefault().inDaylightTime(new Date()) ? TimeZone.getDefault().getDSTSavings() : 0));
        Date now = new Date();

        long differenceInMins = (now.getTime() - dateCreated.getTime()) / 60000;

        if (differenceInMins <= 1)
            return "Less than 1 minute ago";
        else if (1 <= differenceInMins && differenceInMins <= 60)
            return differenceInMins + "  minutes ago";
        else if (60 <= differenceInMins && differenceInMins <= 89)
            return "About 1 hour ago";
        else if (90 <= differenceInMins && differenceInMins <= 1439)
            return "About " + (differenceInMins / 60) + " hours ago";
        else if (1440 <= differenceInMins && differenceInMins <= 2879)
            return "1 day ago";
        else if (2880 <= differenceInMins && differenceInMins <= 43199)
            return (differenceInMins / 1440) + " days ago";
        else if (43200 <= differenceInMins && differenceInMins <= 86399)
            return "About 1 month ago";
        else if (86400 <= differenceInMins && differenceInMins <= 525599)
            return "About " + (differenceInMins / 43200) + " months ago";
        else
            return "About " + (differenceInMins / 525600) + " years ago";
    }
}
