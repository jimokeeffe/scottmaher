package com.mobanode;

import android.app.Application;
import android.content.Context;

import com.mobanode.scottmaher.GalleryActivity;
import com.mobanode.scottmaher.HomeActivity;
import com.mobanode.scottmaher.SocialActivity;
import com.mobanode.social.SocialUtils;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.cache.LruBitmapCache;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;

/**
 * Created by Jims on 15/09/2014.
 */
public class ScottMaherApplication extends Application {

    private static ImageManager imageManager;

    private static Context appCon;

    @Override
    public void onCreate() {
        super.onCreate();

        LoaderSettings settings = new LoaderSettings.SettingsBuilder()
                .withDisconnectOnEveryCall(true)
                .withCacheManager(new LruBitmapCache(this, 15))
                .withExpirationPeriod(Long.parseLong("86400000"))
                .build(this);

        imageManager = new ImageManager(settings);

        Parse.initialize(this, SocialUtils.PARSE_APPLICATION_ID, SocialUtils.PARSE_CLIENT_KEY);
        PushService.subscribe(this, "", HomeActivity.class);
        PushService.setDefaultPushCallback(this, HomeActivity.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        appCon = this;
    }

    public static final ImageManager getImageManager() {

        return imageManager;
    }


}


