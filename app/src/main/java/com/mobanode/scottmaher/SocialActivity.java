package com.mobanode.scottmaher;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mobanode.scottmaher.R;
import com.mobanode.social.SocialCache;
import com.mobanode.social.adapter.FBListAdapter;
import com.mobanode.social.adapter.TwitterListAdapter;
import com.mobanode.social.adapter.YouTubeListAdapter;
import com.mobanode.social.entity.FbPost;
import com.mobanode.social.entity.Tweet;
import com.mobanode.social.entity.YoutubePost;
import com.mobanode.social.fragment.SocialDialogFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.net.URLEncoder;
import java.util.ArrayList;


public class SocialActivity extends FragmentActivity {

    private ListView socialListView;
    private ProgressDialog mProgress;
    private ImageButton twitBtn;
    private ImageButton fbBtn;
    private ImageButton youtubeBtn;

    /* Twitter */
    private String tweetID;

    /* Facebook */
    private boolean fbFeed = false;
    private boolean ytFeed = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);

    socialListView = (ListView) findViewById(R.id.social_list_view);
    socialListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (fbFeed) {
                if (!SocialCache.facebookPosts.get(position).getLink().equals(""))
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SocialCache.facebookPosts.get(position).getLink())));
            } else if (ytFeed) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SocialCache.youtubePosts.get(position).getVidUrl())));
            } else {
                tweetID = SocialCache.twitterPosts.get(position).getTweetId();
                showSocialDialogFragment(tweetID);
            }
        }
    });
    twitterBtnClick(twitBtn);
}
    private void showSocialDialogFragment(String tweetId) {
        FragmentManager fm = getSupportFragmentManager();
        SocialDialogFragment editNameDialog = SocialDialogFragment.newInstance(tweetId);
        editNameDialog.show(fm, "fragment_edit_name");
    }

    /* Twitter Button click */
    public void twitterBtnClick(View view) {
        view.setEnabled(false);
        fbBtn.setEnabled(true);
        youtubeBtn.setEnabled(true);

        fbFeed = false;
        ytFeed = false;

        if (SocialCache.twitterPosts == null)
            new SocialFeedAsync().execute();
        else
            displayPosts();
    }

    /* Facebook Button click */
    public void facebookBtnClick(View view) {
        view.setEnabled(false);
        twitBtn.setEnabled(true);
        youtubeBtn.setEnabled(true);

        fbFeed = true;
        ytFeed = false; 

        if (SocialCache.facebookPosts == null)
            new SocialFeedAsync().execute();
        else
            displayPosts();
    }

    /* YouTube Button click */
    public void youtubeBtnClick(View view) {
        view.setEnabled(false);
        twitBtn.setEnabled(true);
        fbBtn.setEnabled(true);
        ytFeed = true;
        fbFeed = false;

        if (SocialCache.youtubePosts == null)
            new SocialFeedAsync().execute();
        else
            displayPosts();
    }

    public void displayPosts() {
        if (fbFeed)
            socialListView.setAdapter(new FBListAdapter(this, R.layout.list_item_facebook, SocialCache.facebookPosts, SocialCache.fbImgUrls));
        else if (ytFeed)
            socialListView.setAdapter(new YouTubeListAdapter(this, R.layout.list_item_youtube, SocialCache.youtubePosts));
        else
            socialListView.setAdapter(new TwitterListAdapter(this, R.layout.list_item_twitter, SocialCache.twitterPosts));
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.social, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SocialFeedAsync extends AsyncTask<Void, Void, Void> {
        private String strResponse = "";

        @Override
        protected void onPreExecute() {
            if (fbFeed)
                mProgress = ProgressDialog.show(SocialActivity.this, "Please wait...", "Downloading Facebook Posts", true, true);
            else if (ytFeed)
                mProgress = ProgressDialog.show(SocialActivity.this, "Please wait...", "Downloading YouTube Feed", true, true);
            else
                mProgress = ProgressDialog.show(SocialActivity.this, "Please wait...", "Downloading Tweets", true, true);
            mProgress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (fbFeed)
                getFacebookFeed();
            else if (ytFeed)
                getYoutubeFeed();
            else
                getTwitterFeed();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgress.dismiss();
            displayPosts();
        }

        private void getTwitterFeed() {
            Tweet.parseTwitterJson(Tweet.getNewTwitterFeed());
        }

        private void getFacebookFeed() {
            try {
                String accessTokenURL = "https://graph.facebook.com/oauth/access_token?client_id=" + FbPost.APP_ID + "&client_secret=" + FbPost.APP_SECRET + "&grant_type=client_credentials&limit=25";
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(accessTokenURL);
                HttpResponse response = httpClient.execute(httpGet);
                String accessToken = EntityUtils.toString(response.getEntity());
                accessToken = accessToken.split("=")[1];

                String fbURL = "https://graph.facebook.com/" + FbPost.PAGE_ID + "/posts?access_token=" + URLEncoder.encode(accessToken, "UTF-8");
                httpGet = new HttpGet(fbURL);
                response = httpClient.execute(httpGet);
                String jsonRes = EntityUtils.toString(response.getEntity());

                FbPost.parseFBJson(jsonRes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void getYoutubeFeed() {
            try {
                String ytUrl = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=25&playlistId=UU_5TrJxb1CzVec1lCBDeUuQ&key=AIzaSyCmgqVSr3ObkrNgEFrofKMqi2eRLRop7WI";
//                String ytUrl = "http://gdata.youtube.com/feeds/api/users/" + YoutubePost.YT_NAME + "/favorites?&v=2&max-results=24&alt=jsonc";
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(ytUrl);
                HttpResponse response = httpClient.execute(httpGet);
                strResponse = EntityUtils.toString(response.getEntity());

//                YoutubePost.parseYoutubeJSON(strResponse);
                YoutubePost.parseYouTubeV3Json(strResponse);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
