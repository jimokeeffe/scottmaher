package com.mobanode.scottmaher;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import twitter4j.TwitterBase;


public class HomeActivity extends Activity {
    private String tweetID;
    private ListView socialListView;

    public static final String TWITTER_NAME = "scottmahermusic";
    public static final String TWEET_URL = "https://mobile.twitter.com/" + TWITTER_NAME + "/status/";


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        ImageButton button = (ImageButton) findViewById(R.id.TwitterButton);
        button.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(HomeActivity.this, TwitterActivity.class);
                                          startActivity(intent);
                                      }
              }
        );
        ImageButton FaceBook = (ImageButton) findViewById(R.id.FaceBookButton);
        FaceBook.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(HomeActivity.this, FacebookActivity.class);
                                          startActivity(intent);
                                      }
                                  }
        );
        ImageButton Youtube = (ImageButton) findViewById(R.id.youtubeButton);
        Youtube.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(HomeActivity.this, YouTubeActivity.class);
                                          startActivity(intent);
                                      }
                                  }
        );
        ImageButton Gig = (ImageButton) findViewById(R.id.gigButton);
        Gig.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           Intent intent = new Intent(HomeActivity.this, GigActivity.class);
                                           startActivity(intent);
                                       }
                                   }
        );
        ImageButton gallery = (ImageButton) findViewById(R.id.galleryButton);
        gallery.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       Intent intent = new Intent(HomeActivity.this, GalleryActivity.class);
                                       startActivity(intent);
                                   }
                               }
        );
        ImageButton music = (ImageButton) findViewById(R.id.musicButton);
        music.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       Intent intent = new Intent(Intent.ACTION_VIEW);
                                       intent.setData(Uri.parse("https://play.google.com/store/music/artist/Scott_Maher?id=Adzc7bub5zdelyo5dtvjjnhwqsa&hl=en"));
                                       startActivity(intent);
                                   }
                               }
        );
        ImageButton about = (ImageButton) findViewById(R.id.aboutScott);
        about.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           Intent intent = new Intent(HomeActivity.this, AboutActivity.class);
                                           startActivity(intent);
                                       }
                                   }
        );
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);


    }


}





