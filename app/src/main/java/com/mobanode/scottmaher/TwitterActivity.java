package com.mobanode.scottmaher;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.AdapterView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.ImageButton;
import android.widget.ListView;
import com.mobanode.social.SocialCache;
import com.mobanode.social.adapter.TwitterListAdapter;
import com.mobanode.social.entity.Tweet;
import com.mobanode.social.fragment.SocialDialogFragment;

import static com.mobanode.social.entity.Tweet.getNewTwitterFeed;


public class TwitterActivity extends FragmentActivity {
    /* Twitter */
    private String tweetID;
    private ListView socialListView;
    private ProgressDialog mProgress;

    public static final String TWITTER_NAME = "scottmahermusic";
    public static final String TWEET_URL = "https://mobile.twitter.com/" + TWITTER_NAME + "/status/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        socialListView = (ListView) findViewById(R.id.social_list_view);
        socialListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    tweetID = SocialCache.twitterPosts.get(position).getTweetId();
                    showSocialDialogFragment(tweetID);
                }

        });

        showTwitterFeed();
    }

    private void showSocialDialogFragment(String tweetId) {
        FragmentManager fm = getSupportFragmentManager();
        SocialDialogFragment editNameDialog = SocialDialogFragment.newInstance(tweetId);
        editNameDialog.show(fm, "fragment_edit_name");
    }

    public void showTwitterFeed() {



        if (SocialCache.twitterPosts == null)
            new SocialFeedAsync().execute();
        else
            displayPosts();
    }

    public void displayPosts() {

        socialListView.setAdapter(new TwitterListAdapter(this, R.layout.list_item_twitter, SocialCache.twitterPosts));
    }

    public class SocialFeedAsync extends AsyncTask<Void, Void, Void> {
        private String strResponse = "";

        @Override
        protected void onPreExecute() {
            mProgress = ProgressDialog.show(TwitterActivity.this, "Please wait...", "Downloading Tweets", true, true);
            mProgress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            getTwitterFeed();
            return  null ;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgress.dismiss();
            displayPosts();
        }

        private void getTwitterFeed() {
            Tweet.parseTwitterJson(Tweet.getNewTwitterFeed());
        }
    }
}
