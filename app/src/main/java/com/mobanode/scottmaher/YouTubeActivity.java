package com.mobanode.scottmaher;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.mobanode.social.SocialCache;
import com.mobanode.social.adapter.FBListAdapter;
import com.mobanode.social.adapter.TwitterListAdapter;
import com.mobanode.social.adapter.YouTubeListAdapter;
import com.mobanode.social.entity.YoutubePost;
import com.mobanode.social.fragment.SocialDialogFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


public class YouTubeActivity extends FragmentActivity {

    private ListView socialListView;
    private ProgressDialog mProgress;
    private boolean ytFeed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);

        socialListView = (ListView) findViewById(R.id.social_list_view);
        socialListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SocialCache.youtubePosts.get(position).getVidUrl())));
            }
        });



            youtubeBtnClick();
        }



//    private void showSocialDialogFragment(String tweetId) {
//        FragmentManager fm = getSupportFragmentManager();
//        SocialDialogFragment editNameDialog = new SocialDialogFragment(tweetId);
//        editNameDialog.show(fm, "fragment_edit_name");
//    }

    /* YouTube Button click */
    public void youtubeBtnClick() {

        ytFeed = true;

        if (SocialCache.youtubePosts == null)
            new SocialFeedAsync().execute();
        else
            displayPosts();
    }

    public void displayPosts() {
        socialListView.setAdapter(new YouTubeListAdapter(this, R.layout.list_item_youtube, SocialCache.youtubePosts));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.you_tube, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public class SocialFeedAsync extends AsyncTask<Void, Void, Void> {
        private String strResponse = "";


        @Override
        protected void onPreExecute() {
            mProgress = ProgressDialog.show(YouTubeActivity.this, "Please wait...", "Downloading YouTube Feed", true, true);
            mProgress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            getYoutubeFeed();
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgress.dismiss();
            displayPosts();
        }

        public void getYoutubeFeed() {
            try {
               String ytUrl = "https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UCPCwYrCRDNIKCxaWOdzWkrw&order=date&key=AIzaSyCmgqVSr3ObkrNgEFrofKMqi2eRLRop7WI&maxResults=25";
                //String ytUrl = "http://gdata.youtube.com/feeds/api/users/" + YoutubePost.YT_NAME + "/favorites?&v=2&max-results=24&alt=jsonc";
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(ytUrl);
                HttpResponse response = httpClient.execute(httpGet);
                strResponse = EntityUtils.toString(response.getEntity());

//                YoutubePost.parseYoutubeJSON(strResponse);

                YoutubePost.parseYouTubeV3Json(strResponse);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}



