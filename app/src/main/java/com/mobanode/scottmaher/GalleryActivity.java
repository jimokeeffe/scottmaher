package com.mobanode.scottmaher;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Config;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.mobanode.social.SocialUtils;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import com.mobanode.scottmaher.util.CommonUtils;
import com.mobanode.social.adapter.GridViewGalleryAdapter;


import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;

public class GalleryActivity extends Activity {

    private GridView gridview;
    public static ArrayList<String> fullResUrls;
    public static ArrayList<String> venueStrings;
    public static ArrayList<String> commentStrings;
    private GridViewGalleryAdapter adapter;
    private ProgressDialog mProgress;
    private final int columnWidth = 82;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        fullResUrls = new ArrayList<String>();
        venueStrings = new ArrayList<String>();
        commentStrings = new ArrayList<String>();


            setTitle(getResources().getString(R.string.title_activity_gallery));

        gridview = (GridView) findViewById(R.id.gallery_grid_view);

        gridview.setColumnWidth(CommonUtils.getPixelsFromPadding(this,
                columnWidth));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(GalleryActivity.this,
                        GalleryImageActivity.class);
                intent.putExtra("arrPos", position);
                startActivity(intent);
            }
        });

        mProgress = ProgressDialog.show(this, "Please wait", "Downloading Images");
        getImageObjects();
    }

    private void getImageObjects() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserPhoto");
        query.whereEqualTo("locked", "T");
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {

            @Override
            public void done(List<ParseObject> images, ParseException ex) {
                if (ex == null) {
                    Log.v("parse","size " + images.size());
                    getThumbUrls(images);
                } else {
                    Log.v("PARSE-GALLERY",
                            "Error retrieving photos:: " + ex.getMessage());
                }

            }

        });
    }

    private void getThumbUrls(List<ParseObject> images) {
        ArrayList<String> thumbUrls = new ArrayList<String>();

        for (ParseObject obj : images) {
            ParseFile file = (ParseFile) obj.get("thumbFile");
            thumbUrls.add(file.getUrl());

            ParseFile fullFile = (ParseFile) obj.get("imageFile");
            fullResUrls.add(fullFile.getUrl());
            venueStrings.add(obj.getString("imageName") == null ? "" : obj
                    .getString("imageName"));
            commentStrings.add(obj.getString("comment") == null ? "" : obj
                    .getString("comment"));
        }

        mProgress.dismiss();
        adapter = new GridViewGalleryAdapter(this, thumbUrls);
        gridview.setAdapter(adapter);
    }

    // @Override
    // public boolean onCreateOptionsMenu(Menu menu) {
    // // Inflate the menu; this adds items to the action bar if it is present.
    // getMenuInflater().inflate(R.menu.gallery, menu);
    // return true;
    // }

}
