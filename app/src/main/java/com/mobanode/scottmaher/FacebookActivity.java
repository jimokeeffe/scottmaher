package com.mobanode.scottmaher;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import com.mobanode.social.SocialCache;
import com.mobanode.social.adapter.FBListAdapter;
import com.mobanode.social.adapter.TwitterListAdapter;
import com.mobanode.social.adapter.YouTubeListAdapter;
import com.mobanode.social.entity.FbPost;
import com.mobanode.social.entity.Tweet;
import com.mobanode.social.entity.YoutubePost;
import com.mobanode.social.fragment.SocialDialogFragment;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.net.URLEncoder;

import com.mobanode.social.SocialCache;


public class FacebookActivity extends FragmentActivity {
    private ListView socialListView;
    private ProgressDialog mProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
        socialListView = (ListView) findViewById(R.id.social_list_view);

        socialListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SocialCache.facebookPosts.get(position).getLink().equals("");
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(SocialCache.facebookPosts.get(position).getLink())));

            }
        });
        facebookBtnClick();
    }





    public void facebookBtnClick() {
        //view.setEnabled(false);

        if (SocialCache.facebookPosts == null)
            new SocialFeedAsync().execute();
        else
            displayPosts();
    }

    public void displayPosts() {
        socialListView.setAdapter(new FBListAdapter(this, R.layout.list_item_facebook, SocialCache.facebookPosts, SocialCache.fbImgUrls));
    }

    private void getFacebookFeed() {
        try {
            String accessTokenURL = "https://graph.facebook.com/oauth/access_token?client_id=" + FbPost.APP_ID + "&client_secret=" + FbPost.APP_SECRET + "&grant_type=client_credentials&limit=25";
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(accessTokenURL);
            HttpResponse response = httpClient.execute(httpGet);
            String accessToken = EntityUtils.toString(response.getEntity());
            accessToken = accessToken.split("=")[1];

            String fbURL = "https://graph.facebook.com/" + FbPost.PAGE_ID + "/posts?access_token=" + URLEncoder.encode(accessToken, "UTF-8");
            httpGet = new HttpGet(fbURL);
            response = httpClient.execute(httpGet);
            String jsonRes = EntityUtils.toString(response.getEntity());

            FbPost.parseFBJson(jsonRes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class SocialFeedAsync extends AsyncTask<Void, Void, Void> {
        private String strResponse = "";

        @Override
        protected void onPreExecute() {
            mProgress = ProgressDialog.show(FacebookActivity.this, "Please wait...", "Downloading Facebook Posts", true, true);

        }

        @Override
        protected Void doInBackground(Void... params) {
            getFacebookFeed();

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgress.dismiss();
            displayPosts();
        }
    }
}





