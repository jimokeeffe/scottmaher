package com.mobanode.scottmaher.util;

import android.content.Context;

/**
 * Created by Jims on 15/09/2014.
 */
public class CommonUtils {

    public static int getPixelsFromPadding(Context context, int padding) {
        final float scale = context.getResources().getDisplayMetrics().density;
        int widthInPx = (int) (padding * scale + 0.5f);
        return widthInPx;
    }
}
